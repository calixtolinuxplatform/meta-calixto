CALIXTO_IMAGE_PATH_COMMON = '${DEPLOY_DIR}/../../calixto-images/${MACHINE}/${IMAGE_BASENAME}'
CALIXTO_IMAGE_PATH_NAND = '${DEPLOY_DIR}/../../calixto-images/${MACHINE}/${IMAGE_BASENAME}/nand'
CALIXTO_IMAGE_PATH_EMMC  = '${DEPLOY_DIR}/../../calixto-images/${MACHINE}/${IMAGE_BASENAME}/emmc'
CALIXTO_IMAGE_PATH_SDCARD  = '${DEPLOY_DIR}/../../calixto-images/${MACHINE}/${IMAGE_BASENAME}/sdcard'

create_calixto_stamp_image () {

rm -rf ${CALIXTO_IMAGE_PATH_COMMON}/*

mkdir -p ${CALIXTO_IMAGE_PATH_NAND}
mkdir -p ${CALIXTO_IMAGE_PATH_SDCARD}

rm -rf ${CALIXTO_IMAGE_PATH_NAND}/*
rm -rf ${CALIXTO_IMAGE_PATH_SDCARD}/*

cp ${DEPLOY_DIR_IMAGE}/MLO ${CALIXTO_IMAGE_PATH_SDCARD}
cp ${DEPLOY_DIR_IMAGE}/u-boot.img ${CALIXTO_IMAGE_PATH_SDCARD}
cp ${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.tar ${CALIXTO_IMAGE_PATH_SDCARD}/rootfs.tar
cp ${DEPLOY_DIR_IMAGE}/create-sdcard-boot.sh ${CALIXTO_IMAGE_PATH_SDCARD}/
chmod +x ${CALIXTO_IMAGE_PATH_SDCARD}/create-sdcard-boot.sh

}

create_calixto_optima_image() {

rm -rf ${CALIXTO_IMAGE_PATH_COMMON}/*

mkdir -p ${CALIXTO_IMAGE_PATH_EMMC}
mkdir -p ${CALIXTO_IMAGE_PATH_SDCARD}

rm -rf ${CALIXTO_IMAGE_PATH_EMMC}/*
rm -rf ${CALIXTO_IMAGE_PATH_SDCARD}/*

cp ${DEPLOY_DIR_IMAGE}/MLO ${CALIXTO_IMAGE_PATH_SDCARD}
cp ${DEPLOY_DIR_IMAGE}/u-boot.img ${CALIXTO_IMAGE_PATH_SDCARD}
cp ${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.tar ${CALIXTO_IMAGE_PATH_SDCARD}/rootfs.tar
cp ${DEPLOY_DIR_IMAGE}/create-sdcard-boot.sh ${CALIXTO_IMAGE_PATH_SDCARD}/
chmod +x ${CALIXTO_IMAGE_PATH_SDCARD}/create-sdcard-boot.sh

cp ${DEPLOY_DIR_IMAGE}/MLO.byteswap ${CALIXTO_IMAGE_PATH_EMMC}
cp ${DEPLOY_DIR_IMAGE}/u-boot.img ${CALIXTO_IMAGE_PATH_EMMC}
cp ${DEPLOY_DIR_IMAGE}/zImage-am335x-calixto-optima.dtb ${CALIXTO_IMAGE_PATH_EMMC}/am335x-calixto-optima.dtb
cp ${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.tar ${CALIXTO_IMAGE_PATH_EMMC}/rootfs.tar
cp ${DEPLOY_DIR_IMAGE}/u-boot-spl-restore.bin ${CALIXTO_IMAGE_PATH_EMMC}/
cp ${DEPLOY_DIR_IMAGE}/u-boot-restore.img ${CALIXTO_IMAGE_PATH_EMMC}/
cp ${DEPLOY_DIR_IMAGE}/zImage-restore ${CALIXTO_IMAGE_PATH_EMMC}/zImage
}

create_calixto_versa_image() {

rm -rf ${CALIXTO_IMAGE_PATH_COMMON}/*

mkdir -p ${CALIXTO_IMAGE_PATH_EMMC}
mkdir -p ${CALIXTO_IMAGE_PATH_SDCARD}

rm -rf ${CALIXTO_IMAGE_PATH_EMMC}/*
rm -rf ${CALIXTO_IMAGE_PATH_SDCARD}/*

cp ${DEPLOY_DIR_IMAGE}/MLO ${CALIXTO_IMAGE_PATH_SDCARD}
cp ${DEPLOY_DIR_IMAGE}/u-boot.img ${CALIXTO_IMAGE_PATH_SDCARD}
cp ${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.tar ${CALIXTO_IMAGE_PATH_SDCARD}/rootfs.tar
cp ${DEPLOY_DIR_IMAGE}/create-sdcard-boot.sh ${CALIXTO_IMAGE_PATH_SDCARD}/
chmod +x ${CALIXTO_IMAGE_PATH_SDCARD}/create-sdcard-boot.sh

}
