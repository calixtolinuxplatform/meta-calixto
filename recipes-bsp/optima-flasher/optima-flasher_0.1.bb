LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://LICENSE;md5=86ca7e0c52c365016413b6eca889a24a"

inherit deploy

addtask deploy before do_build after do_compile

SRC_URI = "file://LICENSE \
	   file://create-sdcard-boot.sh \
	   file://u-boot-spl-restore.bin \
	   file://u-boot-restore.img \
	   file://zImage-restore \
"

S="${WORKDIR}/"

do_compile () {
	:
}

do_install() {
	:
}

do_deploy() {
	cp ${WORKDIR}/create-sdcard-boot.sh ${DEPLOYDIR}/
	cp ${WORKDIR}/u-boot-spl-restore.bin ${DEPLOYDIR}/
	cp ${WORKDIR}/u-boot-restore.img ${DEPLOYDIR}/
	cp ${WORKDIR}/zImage-restore ${DEPLOYDIR}/
}
