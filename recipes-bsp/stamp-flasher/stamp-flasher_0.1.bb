LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://LICENSE;md5=86ca7e0c52c365016413b6eca889a24a"

inherit deploy

addtask deploy before do_build after do_compile

SRC_URI = "file://LICENSE \
	   file://create-sdcard-boot.sh \
"

S="${WORKDIR}/"

do_compile () {
	:
}

do_install() {
	:
}

do_deploy() {
	cp ${WORKDIR}/create-sdcard-boot.sh ${DEPLOYDIR}/
}
