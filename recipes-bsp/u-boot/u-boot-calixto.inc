UBOOT_SUFFIX ?= "img"
require ${COREBASE}/meta/recipes-bsp/u-boot/u-boot.inc

PACKAGE_ARCH = "${MACHINE_ARCH}"

PROVIDES += "u-boot"
PKG_${PN} = "u-boot"
PKG_${PN}-dev = "u-boot-dev"
PKG_${PN}-dbg = "u-boot-dbg"

S = "${WORKDIR}/git"

# SPL (Second Program Loader) to be loaded over UART , Ethernet or USB RNDIS
SPL_UART_BINARY ?= ""
SPL_UART_IMAGE ?= "${SPL_UART_BINARY}-${MACHINE}-${PV}-${PR}"
SPL_UART_SYMLINK ?= "${SPL_UART_BINARY}-${MACHINE}"

# SPL (Second Program Loader) to be loaded on SPI Flash
SPL_SPI_BINARY ?= ""
SPL_SPI_IMAGE ?= "${SPL_SPI_BINARY}-${MACHINE}-${PV}-${PR}"
SPL_SPI_SYMLINK ?= "${SPL_SPI_BINARY}-${MACHINE}"

do_install_append () {
    if [ "x${SPL_UART_BINARY}" != "x" ]
    then
        install ${S}/spl/${SPL_UART_BINARY} ${D}/boot/${SPL_UART_IMAGE}
        ln -sf ${SPL_UART_IMAGE} ${D}/boot/${SPL_UART_BINARY}
    fi

    if [ "x${SPL_SPI_BINARY}" != "x" ]
    then
        install ${S}/${SPL_SPI_BINARY} ${D}/boot/${SPL_SPI_IMAGE}
        ln -sf ${SPL_SPI_IMAGE} ${D}/boot/${SPL_SPI_BINARY}
    fi

}

do_deploy_append () {
    cd ${DEPLOYDIR}
    if [ "x${SPL_UART_BINARY}" != "x" ]
    then
        install ${S}/spl/${SPL_UART_BINARY} ${DEPLOYDIR}/${SPL_UART_IMAGE}
        rm -f ${DEPLOYDIR}/${SPL_UART_BINARY} ${DEPLOYDIR}/${SPL_UART_SYMLINK}
        ln -sf ${SPL_UART_IMAGE} ${DEPLOYDIR}/${SPL_UART_BINARY}
        ln -sf ${SPL_UART_IMAGE} ${DEPLOYDIR}/${SPL_UART_SYMLINK}
    fi

    if [ "x${SPL_SPI_BINARY}" != "x" ]
    then
        install ${S}/${SPL_SPI_BINARY} ${DEPLOYDIR}/${SPL_SPI_IMAGE}
        rm -f ${DEPLOYDIR}/${SPL_SPI_BINARY} ${DEPLOYDIR}/${SPL_SPI_SYMLINK}
        ln -sf ${SPL_SPI_IMAGE} ${DEPLOYDIR}/${SPL_SPI_BINARY}
        ln -sf ${SPL_SPI_IMAGE} ${DEPLOYDIR}/${SPL_SPI_SYMLINK}
    fi
}
