require u-boot-calixto.inc

# u-boot needs devtree compiler to parse dts files
DEPENDS += "dtc-native"

DESCRIPTION = "u-boot bootloader for TI devices"

LIC_FILES_CHKSUM = "file://Licenses/README;md5=025bf9f768cbcb1a165dbe1a110babfb"
SRC_URI = "https://calixtolinuxplatform@bitbucket.org/calixtolinuxplatform/u-boot-2013.10-calixto.git;branch=${BRANCH}"

BRANCH ?= "master"

SRCREV = "${AUTOREV}"




