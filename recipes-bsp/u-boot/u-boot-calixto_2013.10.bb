require u-boot-calixto.inc

# u-boot needs devtree compiler to parse dts files
DEPENDS += "dtc-native"
DESCRIPTION = "u-boot bootloader for Calixto devices"

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://Licenses/README;md5=025bf9f768cbcb1a165dbe1a110babfb"
SRC_URI[md5sum] = "454a496b2ec8fa2cf60f6b2cba299b10"

PR = "r1"
PV_append = ""

SRC_URI = "git://bitbucket.org/calixtolinuxplatform/u-boot-2013.10-calixto.git;protocol=https;branch=${BRANCH}"

BRANCH ?= "master"

SRCREV = "${AUTOREV}"

SPL_BINARY = "MLO"
SPL_SPI_BINARY = "MLO.byteswap"
SPL_UART_BINARY = "u-boot-spl.bin"
