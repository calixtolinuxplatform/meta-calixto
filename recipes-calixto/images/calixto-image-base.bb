SUMMARY = "calixto-sample-image."


IMAGE_INSTALL += "packagegroup-core-boot ${ROOTFS_PKGMANAGE_BOOTSTRAP} ${CORE_IMAGE_EXTRA_INSTALL} \
		  openssh-ssh \
		  openssh-scp \
		  kernel-modules \
		  u-boot-fw-utils \
		  mtd-utils \
		  mtd-utils-ubifs \
"


IMAGE_LINGUAS = " "

LICENSE = "MIT"

inherit core-image

IMAGE_ROOTFS_SIZE ?= "8192"


