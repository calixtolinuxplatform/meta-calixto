# This file was derived from the linux-yocto-custom.bb recipe in
# oe-core.
#
# linux-yocto-custom.bb:
#
#   A yocto-bsp-generated kernel recipe that uses the linux-yocto and
#   oe-core kernel classes to apply a subset of yocto kernel
#   management to git managed kernel repositories.
#
# Warning:
#
#   Building this kernel without providing a defconfig or BSP
#   configuration will result in build or boot errors. This is not a
#   bug.
#
# Notes:
#
#   patches: patches can be merged into to the source git tree itself,
#            added via the SRC_URI, or controlled via a BSP
#            configuration.
#
#   example configuration addition:
#            SRC_URI += "file://smp.cfg"
#   example patch addition:
#            SRC_URI += "file://0001-linux-version-tweak.patch
#   example feature addition:
#            SRC_URI += "file://feature.scc"
#

inherit kernel
require recipes-kernel/linux/linux-yocto.inc

FILESEXTRAPATHS_append :="${THISDIR}/linux-sitara-ti-${LINUX_VERSION}"

SRC_URI = "git://git.ti.com/sitara-linux/sitara-linux.git;protocol=git;bareclone=1;branch=${KBRANCH}"

SRC_URI += "file://defconfig"

SRC_URI += "file://calixto.scc \
            file://calixto.cfg \
            file://calixto-user-config.cfg \
            file://calixto-user-patches.scc \
	    file://0001-drm-clock-patch.patch \
	    file://am335x-calixto-optima.dts \
	    file://am335x-calixto-nxt.dts \
	    file://am335x-calixto-stamp.dts \
           "

KBRANCH = "sitara-ti-linux-3.14.y"

LINUX_VERSION ?= "3.14.26"

SRCREV="2489c022b2932432606f897741a71b712e6dbe77"

PR = "r0"
PV = "${LINUX_VERSION}+git${SRCPV}"

do_compile_prepend() {

 cp ${WORKDIR}/*.dts ${STAGING_KERNEL_DIR}/arch/arm/boot/dts/
# cp ${WORKDIR}/*.dtsi ${STAGING_KERNEL_DIR}/arch/arm/boot/dts/
 
}

